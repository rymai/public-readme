I'm a Principal Engineer, in the Tenant Scale group (Infrastructure Platforms), working currently on the Cells project. I was formerly specialized in Engineering Productivity (now Developer Experience group).

I'm at GitLab since 2016-01-11.

Read more about me and how I work in [my personal README project](https://gitlab.com/rymai/readme).
